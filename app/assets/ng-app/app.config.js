angular.module('rootApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/users/new', { template: '<user-new></user-new>' }).
        when('/users', { template: '<users-index></users-index>' }).
        when('/users/:userId', { template: '<user-edit></user-edit>' }).
        otherwise('/users');
    }
  ]
);