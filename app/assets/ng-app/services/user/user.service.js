angular.module('services.user')
  .factory("User", ['$resource', 
    function($resource) {
      return $resource('/users/:id.json', {id: "@id"}, {
        update: {
          params: {id: "@id"},
          method: 'PUT'
        }
      });
    }]
  );