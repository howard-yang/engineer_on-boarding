angular.module('userEdit')
  .component('userEdit', {
    templateUrl: '/users/edit.template.html',
    bindings: {
      enableEdit: '=',
      user: '='
    },
    controller: ['User',
      function UserEditController(User) {
        var ctx = this;

        ctx.createUser = function(user) {
          User.sava(user).then(response => { 
            window.location.reload();
          },
          error => {
            ctx.errors = error.data;
          });
        };

        ctx.updateUser = function updateUser() {
          var params = {
            user: {
              first_name: ctx.user.first_name,
              last_name: ctx.user.last_name,
              age: ctx.user.age,
              address: {
                country: ctx.user.address.country,
                address1: ctx.user.address.address1,
                address2: ctx.user.address.address2
              }
            }
          };
            User.update({id: ctx.user.id}, params).$promise
            .then(response => { 
              window.location.reload();
            },
            error => {
              ctx.errors = error.data;
            });
        };
      }
    ]
  });
