angular.module('usersIndex')
  .component('usersIndex', {
    templateUrl: '/users/index.template.html',
    controller: ['User',
      function UsersIndexController(User) {
        this.users = User.query();
        this.enableEdit = false;
        this.user = {};
        var ctx = this;
        this.showEditTemplate = function showEditTemplate(user) {
          ctx.enableEdit = true;
          ctx.user = user;
        }

        this.deleteUser = function deleteUser(user) {
          User.delete({id: user.id}).$promise.then(success => {
            window.location.reload();
          })
        }
      }
    ]
  });
