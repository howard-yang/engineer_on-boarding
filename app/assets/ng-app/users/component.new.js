angular.module('userNew')
  .component('userNew', {
    templateUrl: '/users/new.template.html',
    controller: ['User',
      function UserNewController(User) {
        this.user = {};
        var ctx = this;
        ctx.createUser = function createUser(user) {
          User.save(user).$promise.then(response => { 
            window.location.href = "/users";
          },
          error => {
            ctx.errors = error.data;
          });
        };
      }
    ]
  });
