class User
  include Mongoid::Document

  field :user_id, type: String
  field :first_name, type: String
  field :last_name, type: String
  field :age, type: Integer
  field :gender, type: String
  field :address, type: Hash

  validates :first_name, presence: true, allow_blank: false
  validates :last_name,  presence: true, allow_blank: false
  validates :age, numericality: {other_than: 0}
  validates :gender, inclusion: {in: %w( male female others ), message: "Must be a male or female"}

  has_one :shop
end
