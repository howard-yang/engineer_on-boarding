# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user1 = User.create(id: 1, first_name: 'Howard', last_name: 'Yang')
user2 = User.create(id: 2, first_name: 'Jade', last_name: 'Ding')
user3 = User.create(id: 3, first_name: 'Jake', last_name: 'Ma')
user4 = User.create(id: 4, first_name: 'Tony', last_name: 'Ma', address: {'country': 'sss', address1: 'ss', address2: 'mm'})